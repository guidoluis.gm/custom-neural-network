import numpy as np

class ActivationFunc:
    def __init__(self) -> None:
        pass

    def forward(self, inputs: np.array) -> None:
        pass

    def backward(self, dvalues: np.array) -> None:
        pass


class ReLU(ActivationFunc):
    def forward(self, values: np.array) -> np.array:
        self.inputs = values
        return np.maximum(0, values)
    
    def backward(self, dvalues: np.array) -> None:
        self.dinputs = dvalues.copy()
        self.dinputs[self.inputs <= 0] = 0
        return self.dinputs


class PassBy(ActivationFunc):
    def forward(self, values: np.array) -> np.array:
        return values
    
class Softmax(ActivationFunc):
    def forward(self, values: np.array) -> np.array:
        exp_values = np.exp(values - np.max(values, axis=1, keepdims=True))
        self.output = exp_values / np.sum(exp_values, axis=1, keepdims=True)
        return self.output
    
    def backward(self, dvalues: np.array) -> None:
        
        self.dinputs = np.empty_like(dvalues)

        for idx, (output, dvalue) in enumerate(zip(self.output, dvalues)):

            output = output.reshape(-1, 1)

            jacobian_matrix = np.diagflat(output) - \
                              np.dot(output, output.T)
            
            self.dinputs[idx] = np.dot(jacobian_matrix, dvalue)

        return self.dinputs