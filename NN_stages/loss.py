import numpy as np

class Loss:
    def calculate_loss(y_pred: np.array, y_true: np.array) -> float:
        pass

    def forward(y_pred: np.array, y_true: np.array) -> np.array:
        pass

    def backward(dvalues: np.array, y_true: np.array) -> np.array:
        pass

class CELoss(Loss):
    def calculate_loss(y_pred: np.array, y_true: np.array) -> float:
        return np.mean(CELoss.forward(y_pred, y_true))
    
    def forward(y_pred: np.array, y_true: np.array) -> np.array:
        y_pred_clip = np.clip(y_pred, 1e-7, 1 - 1e-7)

        return -np.log(
            CELoss.get_correct_confidences(
                            y_pred_clip, 
                            y_true)
                        )

    def backward(dvalues: np.array, y_true: np.array) -> np.array:
        samples = len(dvalues)

        if len(y_true.shape) == 1:
            n_labels = dvalues.shape[1]
            y_true = np.eye(n_labels)[y_true]

        dinputs = -y_true/dvalues
        return dinputs/samples

    def get_correct_confidences(y_pred: np.array, y_true: np.array) -> np.array:
        samples = y_true.shape[0]
        match len(y_true.shape):
            case 1:
                return y_pred[range(samples), y_true]
            case 2:
                return np.sum(y_pred*y_true, axis=1)
            case _:
                raise TypeError