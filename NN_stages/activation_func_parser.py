from NN_stages.activation_functions import *

class ActivationFunctionParser:
    
    def to_class(activation_func: str) -> ActivationFunc:
        match activation_func:
            case 'ReLU':
                return ReLU
            case 'Softmax':
                return Softmax
            case _:
                return None