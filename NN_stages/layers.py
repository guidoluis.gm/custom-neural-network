from NN_stages.activation_functions import ActivationFunc
import numpy as np

class Layer:
    def __init__(
            self, 
            input_size: int, 
            output_size: int, 
            activation_func: ActivationFunc
        ) -> None:
        
        self.weights = np.random.randn(input_size, output_size) * 0.01
        self.biases = np.zeros((1, output_size))
        self.activation_func = activation_func

    def forward(self, input: np.array) -> None:
        pass

    def backward(self, dvalues: np.array) -> None:
        pass

    def __str__(self) -> str:
        return f'inputs: {self.weights.shape[0]} | outputs: {self.weights.shape[1]}'
    
class Dense(Layer):
    def forward(self, input: np.array) -> np.array:
        self.inputs = input
        self.output = np.dot(self.input, self.weights) + self.biases
        return self.activation_func.forward(self.output)
    
    def backward(self, dvalues: np.array) -> None:
        self.dweights = np.dot(self.inputs.T, dvalues)
        self.dbiases = np.sum(dvalues, axis=0, keepdims=True)

        self.dinputs = np.dot(dvalues, self.weights.T)