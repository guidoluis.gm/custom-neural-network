from NN_stages.activation_func_parser import ActivationFunctionParser
from NN_stages.loss import *
from NN_stages.layers import *
from NN_stages.metrics import *

class ArtificialNeuralNetwork:
    def __init__(self) -> None:
        self.layers: list[Layer] = []
        self.loss = CELoss
    
    def add_dense_layer(
            self, 
            inputs: int, 
            outputs: int, 
            activation_func: str = 'ReLU'
        ) -> None:

        activation_func = ActivationFunctionParser.to_class(activation_func)
        self.layers.append(Dense(inputs, outputs, activation_func))

    def get_loss(self, y_pred: np.array, y_true: np.array) -> float:
        return self.loss.calculate_loss(y_pred, y_true)
    
    def get_acc(self, y_pred: np.array, y_true: np.array) -> float:
        return Accuracy.calculate(y_pred, y_true)
    
    def predict(self, X: np.array) -> np.array:
        layer_input = X
        for layer in self.layers:
            layer_input = layer.forward(layer_input)
        return layer_input

    def __str__(self) -> str:
        return ''.join([f'layer: {l}\n' for l in self.layers])