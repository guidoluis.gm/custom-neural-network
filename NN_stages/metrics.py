import numpy as np

class Metric:
    def calculate(y_pred: np.array, y_true: np.array) -> float:
        pass

    def get_preds(y_true: np.array) -> np.array:
        match len(y_true.shape):
            case 1:
                return y_true
            case 2:
                return np.argmax(y_true, axis=1)
            case _:
                raise TypeError
            
class Accuracy(Metric):
    def calculate(y_pred: np.array, y_true: np.array) -> float:
        return np.mean(np.argmax(y_pred, axis=1) == Metric.get_preds(y_true))