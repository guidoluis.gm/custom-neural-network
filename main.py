from NN_stages.artificial_neural_network import ArtificialNeuralNetwork
import nnfs
from nnfs.datasets import spiral_data

nnfs.init()

X, y = spiral_data(100, 3)

ann = ArtificialNeuralNetwork()

ann.add_dense_layer(2, 3)
ann.add_dense_layer(3, 3, 'Softmax')


y_pred = ann.predict(X)

print(ann.get_loss(y_pred, y))
print(ann.get_acc(y_pred, y))